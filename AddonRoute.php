<?php
namespace MiniBC\addons\points;

use MiniBC\core\Auth;
use MiniBC\core\Config;
use MiniBC\core\controller\ControllerManager;
use MiniBC\core\entities\Addon;
use MiniBC\core\route\Route;

// define('APP_PATH', dirname(__FILE__)."/storefront/app");

/**
 * Router for Affiliate Only Addon
 *
 * @uses MiniBC\core\Auth
 * @uses MiniBC\core\Config
 * @uses MiniBC\core\entities\Addon
 * @uses MiniBC\core\controller\ControllerManager
 *
 * @extends MiniBC\core\route\Route
 */
class AddonRoute extends Route {

	public $basePath = '';
	public $name = '';
	public $label = '';
	public $addon = null;
	
	protected $customerPath;
	/**
	 * setup routes for this addon
	 * @param 	MiniBC\core\entities\Addon 	$addon instance of the addon object
	 */
	public function __construct(Addon $addon) {
		$this->name = $addon->name;
		$this->addon = $addon;

		// setup paths
		$customerBasePath = Config::get('routes::customer');
		$adminBasePath = Config::get('routes::admin');
		$addonBasePath = Config::get('routes::addon');
		$this->basePath = $addonBasePath . '/points';
		$this->customerPath = $customerBasePath . $this->basePath;

		// setup controllers
		$ordersController = ControllerManager::get('Orders@points');
		$customersController = ControllerManager::get('Customers@points');
		$referralController = ControllerManager::get('Referral@points');
		$settingsController = ControllerManager::get('Settings@points');
		$reportsController = ControllerManager::get('Reports@points');
		$emailController = ControllerManager::get('Emails@points');
		$storefrontController = ControllerManager::get('Storefront@points');
		$webhookController = ControllerManager::get('Webhook@points');

		// Customer Controller
		$pointsRedeemResourcePath = $this->customerPath . '/pointsRedeems';
		$this->get($pointsRedeemResourcePath, array($customersController, 'getRedeemRecords'));

		// Order Controller
		$pointsResourcePath = $this->customerPath . '/pointsOrders';
		$this->get($pointsResourcePath, array($ordersController, 'getOrders'));
		$this->get($pointsResourcePath . '/{id:.+}', array($ordersController, 'getOrders'));
		$this->put($pointsResourcePath . '/{id:.+}', array($ordersController, 'updateOrder'));
		$this->post($pointsResourcePath . '/createPtsOrder', array($ordersController, 'createPtsOrder'));
	
		$this->post($this->customerPath . '/createPtsOrder', array($ordersController, 'createPtsOrder'));

		// Referral Controller
		$pointsReferralResourcePath = $this->customerPath . '/pointsReferrals';
		$this->get($pointsReferralResourcePath, array($referralController, 'getReferralOrders'));
		$this->get($pointsReferralResourcePath . '/{id:.+}', array($referralController, 'getReferralOrders'));
		$this->put($pointsReferralResourcePath . '/{id:.+}', array($referralController, 'updateReferralOrders'));

		// Settings Controller
		$pointsSettingsControllerPath = $this->customerPath . '/pointsSettings';
		$this->get($pointsSettingsControllerPath, array($settingsController, 'getSettings'));
		$this->put($pointsSettingsControllerPath . '/{id:.+}', array($settingsController, 'updateSettings'));

		// Reports Controller
		$this->get($this->customerPath . '/chartDefaultDate', array($reportsController, 'getDefaultChartDates'));
		$this->get($this->customerPath . '/charts/{chartType:.+}', array($reportsController, 'getChartData'));

		// Custom Email Controller
		$customeEmailControllerPath = $this->customerPath . '/pointsEmails';
		$this->get($customeEmailControllerPath, array($emailController, 'getEmails'));
		$this->get($customeEmailControllerPath . '/{id:.+}', array($emailController, 'getEmail'));
		$this->put($customeEmailControllerPath . '/{id:.+}', array($emailController, 'updateEmail'));

		// StoreFront Controller
		$this->get($this->basePath . '/customerInfo', array($storefrontController, 'getCustomerInfo'));
		$this->post($this->basePath . '/redeemStoreCredit', array($storefrontController, 'redeemToStoreCredit'));
		$this->post($this->basePath . '/redeemCoupon', array($storefrontController, 'redeemToCoupon'));

		$this->post($this->basePath . '/storefront/css', array($storefrontController, 'getDefaultStylesheet'));

		// Webhook Controller
		$this->post($this->basePath . '/webhook', array($webhookController, 'createWebhook'));			
		// $this->post($this->customerPath . '/createPtsCustomer', array($webhookController, 'createPtsCustomer'));
		$this->put($this->customerPath . '/updateProductOptionSet', array($webhookController, 'updateProductOptionSet'));	
	}

	protected function methodNotAllowed() {
		// 405 Method Not Allowed
		http_response_code(405);
		exit;
	}

	protected function routeNotFound() {
		// 404 Not Found
		http_response_code(404);
		exit;
	}
}