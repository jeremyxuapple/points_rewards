<?php
namespace MiniBC\addons\recurring\entities;

use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\entities\BaseEntity;
use MiniBC\core\entities\Store;
use MiniBC\core\interfaces\StoreSpecificDataInterface;

/**
 * Class SubscriptionProduct
 *
 * @package MiniBC\addons\recurring\entities
 */
class SubscriptionProduct extends BaseEntity implements StoreSpecificDataInterface
{
	/** @var int unique identifier */
	public $id;

	/** @var int subscription id to assign this product to */
	public $subscriptionId;

	/** @var int product id */
	public $productId;

	/** @var array options selected for this product */
	public $productOptions = array();

	/** @var int product name */
	public $productName = '';

	/** @var string product SKU */
	public $productSku = '';

	/** @var int quantity */
	public $quantity;

	/** @var float product price */
	public $price;

	/** @var float total cost before shipping */
	public $subtotal;

	/** @var float shipping cost */
	public $shipping;

	/** @var float total cost including shipping */
	public $total;

	/** @var bool true if this product should only be attached to the subscription for one recurring payment */
	public $oneTimeOnly = true;

	/** @var \DateTime date the product was added to the subscription */
	public $addedOn;

	/** @var \DateTime last modified date */
	public $lastModified;

	/** @var Store $store */
	public $store;

	/**
	 * Internal Variables
	 */
	protected $customer_id;
	protected $create_time;
	protected $update_time;

	/** @var string database table */
	protected $table = 'rc_subscription_products';

	/** @var array mapping between properties and database columns */
	protected $schema = array(
		'id'				=> 'id',
		'customer_id'		=> 'customer_id',
		'subscriptionId'	=> 'subscription_id',
		'productId'			=> 'product_id',
		'quantity'			=> 'qty',
		'price'				=> 'price',
		'subtotal'			=> 'subtotal',
		'shipping'			=> 'shipping',
		'total'				=> 'total',
		'oneTimeOnly'		=> 'one_time_only',
		'create_time'		=> 'create_time',
		'update_time'		=> 'update_time'
	);

	/**
	 * SubscriptionProduct constructor.
	 *
	 * @param array $data
	 */
	public function __construct($data = array())
	{
		$this->on('load', function($event, $data) {
			$this->formatDataFromDb();
			$this->loadProductOptions();
			$this->loadProductDetails();
		});

		$this->on('before.save', function($event) {
			$this->prepareDataForDb();
		});

		$this->on('before.update', function($event) {
			$this->prepareDataForDb();
		});

		$this->on('after.save', function($event) {
			$this->saveOptions();
		});

		$this->on('after.update', function($event) {
			$this->saveOptions();
		});

		$this->on('after.delete', function($event) {
			$this->deleteOptions();
		});

		if (!empty($data)) $this->populate($data);
	}

	/**
	 * loads product information
	 *
	 * @throws \MiniBC\core\connection\exception\ConnectionException
	 * @throws \MiniBC\core\connection\exception\UnknownConnectionTypeException
	 */
	public function loadProductDetails()
	{
		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');
		$product = $db->selectFirst(
			'bigbackup_bc_products',
			array(
				'customer_id'	=> $this->customer_id,
				'bc_id'			=> $this->productId
			)
		);

		if (!empty($product)) {
			$this->productSku = $product['sku'];
			$this->productName = $product['name'];
		}
	}

	/**
	 * load selected option values for this product
	 *
	 * @throws \MiniBC\core\connection\exception\ConnectionException
	 * @throws \MiniBC\core\connection\exception\UnknownConnectionTypeException
	 */
	public function loadProductOptions()
	{
		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');
		$options = $db->query(
			'
			SELECT 
				sub.product_option_id, sub.product_option_value, 
				opt.display_name AS product_option_label,
				val.label AS product_value_label
			FROM rc_subscription_product_options sub
			LEFT JOIN rc_subscription_products prod
				ON ( 
					prod.customer_id = sub.customer_id 
					AND prod.id = sub.product_id 
				)
			LEFT JOIN bigbackup_bc_products_options opt
				ON ( 
					sub.customer_id = opt.customer_id 
					AND sub.product_option_id = opt.bc_id 
				)
			LEFT JOIN bigbackup_bc_options_values val
				ON (
					sub.customer_id = val.customer_id
					AND sub.product_option_value = val.bc_id
				)
			WHERE sub.customer_id = :customer_id
				AND sub.product_id = :rc_product_id
			',
			array(
				':customer_id' 		=> $this->customer_id,
				':rc_product_id'	=> $this->id
			)
		);

		if (!empty($options)) {
			$this->productOptions = array_map(function($option) {
				return array(
					'option'	=> array(
						'id'	=> $option['product_option_id'],
						'label'	=> $option['product_option_label']
					),

					'value'	=> array(
						'id'	=> $option['product_option_value'],
						'label'	=> $option['product_value_label']
					)
				);
			}, $options);
		}
	}

	/**
	 * save selected option values for this product
	 *
	 * @throws \MiniBC\core\connection\exception\ConnectionException
	 * @throws \MiniBC\core\connection\exception\UnknownConnectionTypeException
	 */
	public function saveOptions()
	{
		if (!empty($this->productOptions)) {
			/** @var MySQLConnection $db */
			$db = ConnectionManager::getInstance('mysql');
			$db->delete(
				'rc_subscription_product_options',
				array( 'product_id' => $this->id )
			);

			foreach ($this->productOptions as $option) {
				if (
					empty($option['option'])
					|| empty($option['value'])
					|| empty($option['option']['id'])
					|| empty($option['value']['id'])
				) {
					continue;
				}

				$db->insert('rc_subscription_product_options', array(
					'product_id'			=> $this->productId,
					'customer_id'			=> $this->customer_id,
					'product_option_id'		=> $option['option']['id'],
					'product_option_value'	=> $option['value']['id']
				));
			}
		}
	}

	/**
	 * delete option values for this product
	 *
	 * @throws \MiniBC\core\connection\exception\ConnectionException
	 * @throws \MiniBC\core\connection\exception\UnknownConnectionTypeException
	 */
	public function deleteOptions()
	{
		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');
		$db->delete(
			'rc_subscription_product_options',
			array( 'product_id' => $this->id )
		);
	}

	/**
	 * format information from database
	 */
	public function formatDataFromDb()
	{
		$this->id = (int)$this->id;
		$this->oneTimeOnly = (bool)$this->oneTimeOnly;

		// convert create and update time to \DateTime object
		if (!empty($this->create_time)) {
			$this->addedOn = \DateTime::createFromFormat('U', $this->create_time);
		}

		if (!empty($this->update_time)) {
			$this->lastModified = \DateTime::createFromFormat('U', $this->create_time);
		}
	}

	/**
	 * set internal variables to be committed to database
	 */
	public function prepareDataForDb()
	{
		if ($this->store instanceof Store) {
			$this->customer_id = $this->store->id;
		}

		if ($this->addedOn instanceof \DateTime) {
			$this->create_time = (int)$this->addedOn->format('U');
		}

		if ($this->lastModified instanceof \DateTime) {
			$this->update_time = (int)$this->lastModified->format('U');
		}
	}

	/**
	 * set the store this subscription belongs to
	 *
	 * @param Store $store
	 */
	public function setStore(Store $store)
	{
		$this->store = $store;
		$this->customer_id = $store->id;
	}
}