 /**
 * populate the data after the page loaded
 */
if(typeof MINIBC !== 'undefined' && $('#customizer').length > 0) {
  var globalmbc = MINIBC;
  var app = 'points';
  var dataSet = {
    customer_id: bc_customer_id
  }

  globalmbc.request(dataSet, '/apps/' + app + '/customerInfo', 'GET', 'json').then(function(data) {

    // populate points into the account page and cart page banner

  	$('.points-available').text(data.points_remaining);

    if (data.points_remaining <= 0) {
      $('.sf-points-info').hide();
    }

    // static url for staging store
    var referral_link = 'http://pointsapp3.mybigcommerce.com/points/?referrer_code=' + data.referral_code;
    $('#referral-link').text(referral_link);
    $('#redeem-ratio')[0].innerText = "Currenly, the redeem ratio is at " + data.redeem_ratio + " points to 1 dollar";
    var orderString = '';
    var referralOrderString = '';
    var redemptionRecordString = '';
  	// if there is order belong to this customer
  	if (data.orders.length > 0) {

  		var orders = data.orders;

  		orderString += "<ul class='list-head'><li>Order ID</li><li>Order Total</li><li>Points Received</li><li>Points Used</li><li>Date</li></ul>";
  		
  		for (var i = 0; i < orders.length; i++ ) {
  			var order_id = orders[i]["order_id"];
  			var order_total = orders[i]["total"];
  			var points_received = orders[i]["points_received"];
  			var points_used = orders[i]["points_used"];
  			var create_time = orders[i]["create_time"];
  			var orderItemString = "<ul class='list-body'><li>" 
  														+ order_id 
  														+ "</li><li>$" 
  														+ order_total 
  														+ "</li><li>" 
  														+ points_received
  														+ "</li><li>" 
  														+ points_used
  														+ "</li><li>" 
  														+ create_time
  														+ "</li></ul>";

  			orderString += orderItemString;
  		}

  		$('.orderInfo').append(orderString);
  	}

  	// if there is referral belong to this customer
  	if (data.referralRecords.length > 0) {

  		var referralOrders = data.referralRecords;

  		referralOrderString += "<ul class='list-head'><li>Order ID</li><li>Order Total</li><li>Bounty Reward</li><li>Referral Ratio Reward</li><li>Total Points Reward</li><li>referee_email</li><li>Date</li></ul>";
  		
  		for (var i = 0; i < referralOrders.length; i++ ) {
  			var order_id = referralOrders[i]["order_id"];
  			var order_total = referralOrders[i]["order_total"];
  			var bounty_reward = referralOrders[i]["bounty_reward"];
  			var referral_ratio_reward = referralOrders[i]["referral_ratio_reward"];
  			var points_reward = referralOrders[i]["points_reward"];
  			var referee_email = referralOrders[i]["referee_email"];
  			var create_time = referralOrders[i]["create_time"];
  			var referralOrderItem = "<ul class='list-body'><li>" 
  														+ order_id 
  														+ "</li><li>$" 
  														+ order_total 
  														+ "</li><li>" 
  														+ bounty_reward
  														+ "</li><li>" 
  														+ referral_ratio_reward
  														+ "</li><li>" 
  														+ points_reward
  														+ "</li><li>" 
  														+ referee_email
  														+ "</li><li>" 
  														+ create_time
  														+ "</li></ul>";

  			referralOrderString += referralOrderItem;
  		}

  		$('.referralInfo').append(referralOrderString);
  	}
 
  	// if there is redemption records belong to this customer
  	if (data.redemptionRecords.length > 0) {

  		var redemptionRecords = data.redemptionRecords;

  		redemptionRecordString += '<ul class="list-head"><li>Points Redeemed</li><li>Dollar Value</li><li>Redempton Options</li><li>Coupon Code</li><li>Date</li></ul>';
  		
  		for (var i = 0; i < redemptionRecords.length; i++ ) {
  			var pts_redeemed = redemptionRecords[i]["pts_redeemed"];
  			var dollar_value = redemptionRecords[i]["dollar_value"];
  			var redemption_option = redemptionRecords[i]["redemption_option"];
  			var coupon_code = redemptionRecords[i]["coupon_code"];
  			var create_time = redemptionRecords[i]["create_time"];
  			var recordItemString = "<ul class='list-body'><li>" 
  														+ pts_redeemed 
  														+ "</li><li>$" 
  														+ dollar_value 
  														+ "</li><li>" 
  														+ redemption_option
  														+ "</li><li>" 
  														+ coupon_code
  														+ "</li><li>" 
  														+ create_time
  														+ "</li></ul>";

  			redemptionRecordString += recordItemString;
  		}
  	}

  	$('.redemptionInfo').append(redemptionRecordString);
  	
  });
 
}(window.jQuery, MINIBC);

/**
* Populate points inforamtion for a customer on the account page and cart page banner
*/

if (window.location.pathname == '/account.php' || window.location.pathname == '/cart.php' && typeof MINIBC !== 'undefined') {
    var globalmbc = MINIBC;
    var app = 'points';
    var dataSet = {
       customer_id: bc_customer_id
    } 
    globalmbc.request(dataSet, '/apps/' + app + '/customerInfo', 'GET', 'json').then(function(data) {
      
      $('.points-available').text(data.points_remaining);
      if (data.points_remaining <= 0) {
        $('.sf-points-info').hide();
      }
    }); 
}

/*
* Load the default style for the accoutn page  
*/

if (typeof MINIBC !== 'undefined') {
    var globalmbc = MINIBC;
    var app = 'points';

    globalmbc.request({}, '/apps/' + app + '/storefront/css', 'POST', 'text')
      .done(function(resp) {
        console.log(resp);
        var defaultCss = $('<style type="text/css"/>');
        defaultCss.html(resp);

        $('head').append(defaultCss);
      }); 
}

