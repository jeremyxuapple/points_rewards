
// document.cookie = "referrer_code=3EU3ZZ; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/;";

// Inject the customer notes if the referrer_code has been set into the cookie
  
if (window.location.pathname == '/checkout.php' && window.get_cookie("referrer_code") != undefined) {
    
  var referrer_code = window.get_cookie("referrer_code");
  // Listen to the ajaxcompete
  $('html').ajaxComplete(function(event,response,url) {

    var response = JSON.parse(response.responseText);

    // execute the function when the customer are on the confirmation step
    if (response.changeStep != undefined && response.changeStep == "Confirmation") {

      $('#OrderConfirmationForm').removeAttr('onsubmit');

      $('#OrderConfirmationForm').submit(function (event) {
        event.preventDefault();
        $('[name="ordercomments"]')[0].value = $('[name="ordercomments"]')[0].value + "\nreferrer_code = " + referrer_code;
        ExpressCheckout.ConfirmPaymentProvider();
      });
    }

  });

}

var bc_customer_id = $('#customer-object')[0].innerText.trim();

// Hide and show the proper information for the customer
if(bc_customer_id == '') {
  $('.login-customer').hide();
} else {
  $('.guest-customer').hide();
}

// populate the data after the page loaded
if(typeof MINIBC !== 'undefined' && $('#customizer').length > 0) {
  var globalmbc = MINIBC;
  var app = 'points';
  var dataSet = {
    customer_id: bc_customer_id
  }

$('#store-credit-submit').unbind('click').click(function(e){
    e.preventDefault();
    var pts_to_redeem = $('#store-credit')[0].value;
    var dataSet = {
      bc_customer_id: bc_customer_id,
      pts_to_redeem: pts_to_redeem
    }
    globalmbc.request(dataSet, '/apps/' + app + '/redeemStoreCredit', 'POST', 'json').then(function(res){
      if(res.success) {
        $('#store-credit-msg')[0].innerText = "Congragulations!!! You have successfully redeemed " + res.dollar_amt + " dollars!";
        $('.storeCredit').show();
      }
    });
});


$('#coupon-submit').unbind('click').click(function(e){
    e.preventDefault();
    var pts_to_redeem = $('#coupon')[0].value;
    var dataSet = {
      bc_customer_id: bc_customer_id,
      pts_to_redeem: pts_to_redeem
    }
    globalmbc.request(dataSet, '/apps/' + app + '/redeemCoupon', 'POST', 'json').then(function(res){
      if(res.success) {
        $('#coupon-msg')[0].innerText = "Congragulations!!! You have successfully redeemed " 
                                        + res.dollar_amt + " dollars! The coupon code is " 
                                        + res.coupon_code + "\n Please keep this code for your record!";
        $('.coupon').show();
      }
    });
});

$('.close').click(function() {
  $('.popUpWrap').hide();
});

}(window.jQuery, MINIBC);
  
// verify if the referrr_code cookie has already exist

  var setCookie = false;
  var cookiearray = document.cookie;
  
  if (window.location.search.includes("referrer_code")) {

    var referrer_code = window.location.search.slice(-6);
    setCookie = true;

    for(var i=0; i<cookiearray.length; i++) {
        var name = $.trim(cookiearray[i].split('=')[0]);
        var value = $.trim(cookiearray[i].split('=')[1]);
        // this means the referrer_code has already been set  
        if (name == "referrer_code" && value == referrer_code) { setCookie = false; break; }
    }

  } 

  if(setCookie) {

      console.log("creating cookie...");
      var expires = new Date();
      expires.setDate(expires.getDate() + 7);

      document.cookie = "referrer_code=" + referrer_code + "; expires=" + expires.toGMTString() + "; domain=mybigcommerce.com; path=/;";
  }



