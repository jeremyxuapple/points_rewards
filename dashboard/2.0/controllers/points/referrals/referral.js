EmberApp.PointsReferralsReferralController = Ember.Controller.extend({

	'app': Ember.inject.controller('points'),

	'actions' : {
		'updateReferralOrder': function (stayOnPage) {
			var controller = this;
			var order = this.get('model');
				order.save().then(function(order) {
                controller.notifications.show(
                    'The order <b>' + order.get('order_id') + '</b> has been updated.',
                    'Order Updated Successfully.',
                    'success'
                );

                if (!stayOnPage) controller.transitionToRoute('points.orders');
            }, function(err) {
                // error saving
                order.rollback();

                var message = 'An error occurred when updating this order. Please try again later. Or maybe the record has not been changed at all. ';

                if (err.errors !== undefined && err.errors.length > 0) {
                    message = err.errors[0];
                }

                controller.notifications.show(
                    message,
                    'Failed to Update Product',
                    'error'
                );
            });

			} // end of updateReferralOrder
			
	}// end of actions
	
		
});