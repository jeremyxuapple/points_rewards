EmberApp.PointsReportsIndexController = Ember.Controller.extend({
    'chartData':[],
    'chartRangeStart': 'yy-mm-dd',
    'chartRangeEnd': 'yy-mm-dd',
    'chartType': 'regularPointsGiven',
    'regularPointsGiven':'...',
    'referralPointsGiven':'...',
    'totalPointsOut':'...',
    'totalPointsRedeem': '...',
    'ptsRedeemStoreCredit': '...', 
    'ptsRedeemCoupon':'...',
    'totalDollarAmt':'...',
    'labelArray':'...',
    'dataArray':'...',
    'dataChart':undefined,
    'datepickerStartOpts': {datepicker:'#startDate .datepicker', altField: '#', altFormat:'selfDate'},
    'datepickerEndOpts': {datepicker:'#endDate .datepicker', altField: '#', altFormat:'selfDate'},

    'init': function() {
        this._super();
    },

    'setupChart':function() {
        var controller = this;
        Ember.$.ajax({
            'url': '/customer/apps/points/chartDefaultDate',
            'type': 'GET',
            // 'data': {'chartRangeStart':controller.get('chartRangeStart'),'chartRangeEnd':controller.get('chartRangeEnd')},
            'dataType': 'json',
            'complete': function(data){
                var dateData = JSON.parse(data.responseText);
                controller.
                    setProperties({
                        chartRangeStart: dateData['chartRangeStart'],
                        chartRangeEnd:dateData['chartRangeEnd']
                });

            }
        });
    },

    'getChartData':function() {
        var controller = this;
        this.set('isLoading', true);

        Ember.$.ajax({
            'url': '/customer/apps/points/charts/'+this.get('chartType'),
            'type': 'GET',
            'data': {
                'chartRangeStart':controller.get('chartRangeStart'),
                'chartRangeEnd':controller.get('chartRangeEnd')
            },
            'dataType': 'json',
            'complete': function(data){
                var chartData = JSON.parse(data.responseText);
                controller
                    .set('chartData',chartData)
                    .set('totalPointsOut',chartData['total_points_out'])
                    .set('totalPointsRedeem',chartData['total_points_redeem'])
                    .set('ptsRedeemStoreCredit',chartData['pts_redeem_store_credit'])
                    .set('ptsRedeemCoupon',chartData['pts_redeem_coupon'])
                    .set('totalDollarAmt',chartData['total_dollar_amt'])
                    .set('regularPointsGiven',chartData['regular_points_given'])
                    .set('referralPointsGiven',chartData['referral_points_given'])
                    .set('labelArray',chartData['label_array'])
                    .set('dataArray',chartData['data_array'])
                    .refreshChart();

                controller.set('isLoading',false);
            }
        });
    },
    
    'refreshChart':function() {
        var dataChart = this.get('dataChart');
        if (dataChart !== undefined) dataChart.destroy();

        var ctx = $("#myChart");
        dataChart = new Chart(ctx, {
            type: 'line',
            maintainAspectRatio: true,
            responsive:true,
            animationEnabled: true, 
            data: {
                labels: this.get('labelArray'),
                datasets: [
                    {
                        fill: 1,
                        label: this.chartName(),
                        backgroundColor: "rgba(246, 160, 138, 0.2)",
                        borderColor: "rgba(246, 160, 138, 1)",
                        pointBorderColor: "rgba(246, 160, 138, 1)",
                        pointBackgroundColor: "rgba(255, 255, 255, 1)",
                        pointBorderWidth: 2,
                        pointHoverRadius: 6,
                        pointHoverBackgroundColor: "rgba(255, 255, 255, 1)",
                        pointHoverBorderColor: "rgba(246, 160, 138, 1)",
                        data: this.get('dataArray')
                    }
                ]
            },
            options: {
                maintainAspectRatio: true,
                responsive:true,
                scales: {
                    xAxes: [{
                        gridLines: { display: false }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: false
                        }
                    }]
                }
            }
        });

        this.set('dataChart',dataChart);
    },

    'chartName': function() {
        var chartType=this.get('chartType');
        if (chartType === "regularPointsGiven") return "Regular Points Given";
        if (chartType === "referralPointsGiven") return "Referral Points Given";
  
    },

    'chartRangeChanged': function() {
        this.getChartData();
    }.observes('chartRangeStart','chartRangeEnd','chartType'),

    'actions': {
        'changeChartType':function(chartType) {
            this.set('chartType',chartType);
        },
    }
});