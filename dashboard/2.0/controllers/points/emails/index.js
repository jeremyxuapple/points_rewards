/**
 * Controller for Recurring Email Listing page
 *
 * @class RecurringEmailsIndexController
 * @namespace EmberApp
 * @extends Ember.Controller
 */
EmberApp.PointsEmailsIndexController = Ember.Controller.extend({
	'app': Ember.inject.controller('points'),

	'actions': {
		'enableEmail': function(email) {
			var controller = this;

			if (email.get('configured') === false) {
				controller.notifications.show(
					'Please setup the email content before enabling the email.',
					'Failed to Enable Email',
					'error'
				);

				return false;
			}

			email.set('enabled', true);
			email.save().then(function(email) {
				controller.notifications.show(
					'<b>' + email.get('name') + '</b> has been <b>enabled</b> and the email will be sent to the customer when the event occurs.',
					'Email Enabled Successfully.',
					'success'
				);
			}, function(err) {
				email.rollbackAttributes();

				var message = 'An error occurred when enabling the <b>' + email.get('name') + '</b> email. Please try again later.';

				if (typeof err.errors !== 'undefined' && err.errors.length > 0) {
					if (typeof err.errors[0] === 'string') {
						message = err.errors[0];
					}
				}

				controller.notifications.show(
					message,
					'Failed to Enable Email',
					'error'
				);
			});
		},

		'disableEmail': function(email) {
			var controller = this;

			email.set('enabled', false);
			email.save().then(function(email) {
				controller.notifications.show(
					'<b>' + email.get('name') + '</b> has been <b>disabled</b> and will no longer be sent to the customer when the event occurs.',
					'Email Disabled Successfully.',
					'success'
				);
			}, function(err) {
				email.rollbackAttributes();

				var message = 'An error occurred when disabling the <b>' + email.get('name') + '</b> email. Please try again later.';

				if (typeof err.errors !== 'undefined' && err.errors.length > 0) {
					if (typeof err.errors === 'string') {
						message = err.errors[0];
					}
				}

				controller.notifications.show(
					message,
					'Failed to Disable Email',
					'error'
				);
			});
		}
	}
});