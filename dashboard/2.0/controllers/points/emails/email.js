EmberApp.PointsEmailsEmailController = Ember.Controller.extend({
	'inSourceMode': false,

	'resetChanges': Ember.on('deactivate', function() {
		console.error('deactivate');
	}),
	'actions': {
		'enterSourceMode': function() {
			this.set('inSourceMode', true);
		},

		'exitSourceMode': function() {
			this.set('inSourceMode', false);
		},

		'saveEmail': function(stayOnPage) {
			var controller = this,
				email = this.get('model');

			if (this.get('inSourceMode') === true) {
				controller.notifications.show(
					'Your are currently editing your email content in <b>Source Mode</b>. Please exit <b>Source Mode</b> by clicking the <b>Source</b> button, then click <b>Save</b> again to save your email.',
					'Failed to Update Email',
					'error'
				);

				return false;
			}

			this.set('saveInProgress', true);

			email.save()
				.then(function(email) {
					controller.notifications.show(
						'The <b>' + email.get('name') + '</b> email has been updated.',
						'Email Updated Successfully.',
						'success'
					);

					if (!stayOnPage) controller.transitionToRoute('points.emails.index');
				}, function(err) {
					// error saving
					email.rollbackAttributes();

					var message = 'An error occurred when updating this email. Please try again later.';

					if (typeof err.errors !== 'undefined' && err.errors.length > 0) {
						if (typeof err.errors[0] === 'string') {
							message = err.errors[0];
						}
					}

					controller.notifications.show(
						message,
						'Failed to Update Email',
						'error'
					);
				})
				.finally(function() {
					controller.set('saveInProgress', false);
				});
		}
	}
});