EmberApp.PointsSettingsIndexController = Ember.Controller.extend({

	'app': Ember.inject.controller('points'),

	'actions':  {
	'updateSettings': function (stayOnPage) {
        var controller = this;
		var settings = this.get('model');

			settings.save().then(function(settings) {
                controller.notifications.show(
                    'The settings has been updated.',
                    'Settings Updated Successfully.',
                    'success'
                );
            }, function(err) {
                // error saving
                var message = 'An error occurred when updating the settings. Please try again later. Or maybe the record has not been changed at all.';

                if (err.errors !== undefined && err.errors.length > 0) {
                    message = err.errors[0];
                }

                controller.notifications.show(
                    message,
                    'Failed to Update Settings',
                    'error'
                );
            });
		}

	}, 

});