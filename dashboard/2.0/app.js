/** 
 * Points App Router
*/

EmberApp.Router.map(function() {

	this.resource('points', { path: '/apps/points'}, function(){

		this.route('orders', { path: '/orders'}, function(){
			this.route('order', { path: '/:id' });
		});

		this.route('referrals', { path: '/referrals'}, function(){
			this.route('referral', { path: '/:id' });
		});

		this.route('redeems', { path: '/redeems'}, function(){});

		this.route('reports', { path: '/reports'}, function(){});

		this.route('emails', { path: '/emails'}, function(){
			this.route('email', { path: '/:id' });
		});
		''
		this.route('settings', { path: '/settings'}, function(){});
		
	});

});


/**
 * Adapter for points Models
 *
 * @class PointsAdapter
 * @extends DS.RESTAdapter
 * @namspace EmberApp
 */
EmberApp.PointsAPIAdapter = DS.RESTAdapter.extend({
    namespace: 'customer/apps/points'
});