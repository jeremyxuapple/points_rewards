/** 
 * Points Model
 *
 * @class Points
 * @extends DS.Model
 * @namespace EmberApp
 *
*/
EmberApp.PointsOrder = DS.Model.extend({
	'customer_store_id' : DS.attr('number'),
	'bc_customer_id': DS.attr('number'),
	'order_id' : DS.attr('number'),
	'total' : DS.attr('number'),
	'cash_total' : DS.attr('number'),
	'points_received' : DS.attr('number'),
	'points_used' : DS.attr('number'),
	'points_remaining' : DS.attr('number'),
	'first_name' : DS.attr('string'),
	'last_name' : DS.attr('string'),
	'email' : DS.attr('string'),
	'pts_update_notes' : DS.attr('string'),
});

EmberApp.PointsOrderAdapter = EmberApp.PointsAPIAdapter.extend({});