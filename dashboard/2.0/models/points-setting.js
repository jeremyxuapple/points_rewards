/** 
 * Points Model
 *
 * @class Points
 * @extends DS.Model
 * @namespace EmberApp
 *
*/
EmberApp.PointsSetting = DS.Model.extend({
	'customer_store_id' : DS.attr('number'),
	'dollar_pts_ratio': DS.attr('number'),	
	'referrer_bounty': DS.attr('number'),	
	'referrer_ratio': DS.attr('number'),
	'email': DS.attr('string'),
	'company_name': DS.attr('string'),		
});

EmberApp.PointsSettingAdapter = EmberApp.PointsAPIAdapter.extend({});