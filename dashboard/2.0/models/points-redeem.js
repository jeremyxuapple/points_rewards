/** 
 * Points Model
 *
 * @class Points
 * @extends DS.Model
 * @namespace EmberApp
 *
*/
EmberApp.PointsRedeem = DS.Model.extend({
	'redemption_option' : DS.attr('string'),
	'coupon_code' : DS.attr('string'),
	'first_name': DS.attr('string'),
	'last_name' : DS.attr('string'),
	'email' : DS.attr('string'),
	'pts_redeemed' : DS.attr('number'),
	'dollar_value' : DS.attr('number'),
	'create_time' : DS.attr('string'),
});

EmberApp.PointsRedeemAdapter = EmberApp.PointsAPIAdapter.extend({});