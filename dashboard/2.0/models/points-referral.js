/** 
 * Points Model
 *
 * @class Points
 * @extends DS.Model
 * @namespace EmberApp
 *
*/
EmberApp.PointsReferral = DS.Model.extend({
	'order_id' : DS.attr('number'),
	'customer_store_id' : DS.attr('number'),
	'referrer_first_name': DS.attr('string'),
	'referrer_last_name' : DS.attr('string'),
	'referrer_email' : DS.attr('string'),
	'referee_first_name' : DS.attr('string'),
	'referee_last_name' : DS.attr('string'),
	'referee_email' : DS.attr('string'),
	'points_reward' : DS.attr('number'),
	'referral_ratio_reward' : DS.attr('number'),
	'bounty_reward' : DS.attr('number'),
	'order_total' : DS.attr('number'),	
	'pts_update_notes' : DS.attr('string'),
});

EmberApp.PointsReferralAdapter = EmberApp.PointsAPIAdapter.extend({});