/**
 * Recurring Payment Email Model
 *
 * @class RcEmail
 * @extends DS.Model
 * @namespace EmberApp
 */
EmberApp.PointsEmail = DS.Model.extend({
	'name': DS.attr('string'),
	'desc': DS.attr('string'),
	'enabled': DS.attr('boolean'),
	'bcc': DS.attr('string'),
	'bccName': DS.attr('string'),
	'subject': DS.attr('string'),
	'content': DS.attr('string'),
	'configured': DS.attr('boolean'),
	'variables': DS.attr('string'),
	'update_time': DS.attr('number')
});

EmberApp.PointsEmailAdapter = EmberApp.PointsAPIAdapter.extend({});