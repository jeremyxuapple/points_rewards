/**
 * Points Route
 *
 * @class PointsRoute
 * @namespace EmberApp
 * @extends Ember.Route
 *
*/
EmberApp.PointsRoute = Ember.Route.extend({
    /**
     * retrieve application information from the data store
     *
     * @function model
     * @returns {RSVP.Promise}
     */

    model: function() {
    	return Ember.RSVP.all([
    		this.store.find('app', 'points')
    	]);
    },

    afterModel:function(model) {
    	var applicationController = this.controllerFor('application');

        var menuItems = appMenuItems.get('points');
        applicationController.set('appsMenu', menuItems);
    },

    setupController: function(controller, model){

    	this._super(model, controller);
    	var app = model.objectAt(0);

    	var menuItems = appMenuItems.get('points');
        
    	controller
    		.set('app', app)
    		.set('appsMenu', menuItems);
    },

    actions: {
    	'clearSearch': function() {
    		 this.controller.send('clearSearch');
    	}
    }


});