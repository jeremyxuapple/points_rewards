EmberApp.PointsRedeemsIndexRoute = Ember.Route.extend({
	'model': function() {
        return Ember.RSVP.all([
            this.store.findAll('pointsRedeem')
        ]);
    },

	 'setupController': function(controller, model) {
        this._super(controller, model.objectAt(0));
    }

});
