EmberApp.PointsReportsIndexRoute = Ember.Route.extend({

    setupController: function(controller, model) {
        this._super(controller, model);
        controller.setupChart();
        controller.getChartData();
    }
    
});