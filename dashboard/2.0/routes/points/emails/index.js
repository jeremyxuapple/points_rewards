EmberApp.PointsEmailsIndexRoute = Ember.Route.extend({
	'model': function() {
        return Ember.RSVP.all([
            this.store.findAll('pointsEmail')
        ]);
    },

    'setupController': function(controller, model) {
		this._super(controller, model.objectAt(0));
        
    }

});