EmberApp.PointsSettingsIndexRoute = Ember.Route.extend({
	'model': function() {
		return Ember.RSVP.all([
				 this.store.findAll('points-setting')
			]);
		},

    'setupController': function(controller, model) {
        this._super(controller, model.objectAt(0));
        controller.set('settings', model.objectAt(0).objectAt(0));
    }
});