/**
 * Recurring Email List
 */
EmberApp.PointsEmailListComponent = Ember.Component.extend({
	'tagName': 'ul',
	'classNames': [ 'rc-email-list', 'table-list' ],
	'emails': false,

	'enableAction': false,
	'disableAction': false,

	'actions': {
		'enable': function(email) {
			if (this.get('enableAction') !== false) {
				this.sendAction('enableAction', email);
			}
		},

		'disable': function(email) {
			if (this.get('disableAction') !== false) {
				this.sendAction('disableAction', email);
			}
		}
	}
});