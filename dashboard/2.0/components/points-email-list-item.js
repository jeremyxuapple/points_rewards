/**
 * Recurring Email List Item
 */
EmberApp.PointsEmailListItemComponent = Ember.Component.extend({
	'tagName': '',
	'email': false,

	// button actions
	'deleteAction': false,
	'enableAction': false,
	'disableAction': false,
	'selectAction': false,
	'index': 0,
	'total': 1,

	'zIndex': function() {
		var total = this.get('total'),
			index = this.get('index');

		return parseInt(total) - parseInt(index);
	}.property('index', 'total'),

	'actions': {
		'enable': function() {
			var action = this.get('enableAction');
			if (action !== false) this.sendAction('enableAction', this.get('email'));
		},

		'disable': function() {
			var action = this.get('disableAction');
			if (action !== false) this.sendAction('disableAction', this.get('email'));
		}
	}
});