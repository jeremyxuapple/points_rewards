<?php

/**
* Email Sevice with Mandrill API
*/

namespace MiniBC\addons\points\services;

use Mandrill;
use MiniBC\core\interfaces\SingletonInterface;

class MandrillEmailService implements SingletonInterface
{
	
	private static $instance;
	# this is the default key - will need to change later
	private $key = "eCjsjeWX5IsJlKWuhnPcGQ";

	public function __construct()
	{
    $this->mandrill = new Mandrill($this->key);

	}

	 /**
	 * Generate the email wiht Mandrill Api 
	 * @param $from - email initializer
	 * @param $to - email receipent
	 * @param $template_name - the template name has been stored in the Mandrill account
	 * @param $template_content - the data needs to be passed inside of the email
	 * 
	*/
	public function sendMailTemplate($from, $to, $subject, $template_name, $template_content) 
	{
		# let's create the recipient array from the to variable
		$recipients = array();
		if (is_array($to)) {
			foreach($to as $recipient) {
				$recipients[] = array(
					"email" => $recipient
				);
			}
		} else {
			$recipients[] = array(
				"email" => $to
			);
		}

		# let's create the message based on the variables
		$message = array(
			"subject" => $subject,
			"from_email" => $from,
			"to" => $recipients,
			"headers" => array("Reply-To" => $from),
			'important' => false,
			"merge" => true,
			'merge_language' => 'mailchimp',
		);

		# some default values
		$async = false;
		$ip_pool = 'Main Pool';

		# now let's try sending the email
		try {
			$result = $this->mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool);
		}
		catch(Exception $e) {
			throw new \Exception("Mandrill Error: ".$e->getMessage);
		}

		return $result;
	}

	public static function getInstance()
	{
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}

		return self::$instance;
	}

}