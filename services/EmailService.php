<?php
namespace MiniBC\addons\points\services;

use MiniBC\addons\points\entities\Subscription;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\connection\exception\ConnectionException;
use MiniBC\core\connection\exception\UnknownConnectionTypeException;
use MiniBC\core\entities\Store;
use MiniBC\core\interfaces\SingletonInterface;
use MiniBC\core\Log;
use MiniBC\core\Mail;
use MiniBC\core\mail\Message;


class EmailService implements SingletonInterface
{
	/** @var EmailService $instance */
	private static $instance;

	/**
	 * sends email to customer when their points has been updated by the store owner
	 *
	 * template tags available for this email:
	 *  - %%CustomerFirstName%%
	 *  - %%CustomerLastName%%
	 *  - %%CustomerEmail%%
	 *  - %%OrderPoints%%
	 *  - %%PointsChangeReason%%
	 *
	 * @param Subscription $subscription
	 * @return bool returns false on failure
	 */
	public function sendRegularOrderPointsUpdateEmail($email_vars, $pts_for_order)
	{		

		extract($email_vars);
    // gather required information to compose email
		$settings = $this->getEmailSettings($customer_store_id);
		$content = $this->getEmailContent('regular_order_points_changes', $customer_store_id);
		$order = $this->getRegularOrder($customer_store_id, $order_id);

		if (!$this->isEmailConfigured($order, $settings, $content)) {
			// missing email settings
			return false;
		}
    

		$templateVariables = array(
			// customer information
			'%%CustomerFirstName%%' => $order['first_name'],
			'%%CustomerLastName%%' 	=> $order['last_name'],
			'%%CustomerEmail%%'		=> $order['email'],
			'%%OrderId%%'					=> $order['order_id'],
			'%%OrderPoints%%'		=> $pts_for_order,
			'%%PointsChangeReason%%' => $pts_update_notes,
		);

		// send email
		$result = Mail::send(
			$content['content'],
			$templateVariables,
			function(Message &$message) use ($content, $settings, $order) {
				//assign sender information
				
				$recipientName = $order['first_name'] . ' ' . $order['last_name'];
	
				$message
					->to($order['email'], $recipientName)
					->from($settings['email'], $settings['company_name'])
					->subject($content['subject']);

				if($content['bcc'] !== '' && $content['bcc'] !== null) {
					$message->bcc($content['bcc'], $content['bcc_name']);
				}
			}
		);
		
		return $result;
	}

		/**
	 * sends email to customer when their points has been updated by the store owner
	 *
	 * template tags available for this email:
	 *  - %%CustomerFirstName%%
	 *  - %%CustomerLastName%%
	 *  - %%CustomerEmail%%
	 *  - %%OrderPoints%%
	 *  - %%PointsChangeReason%%
	 *
	 * @param Subscription $subscription
	 * @return bool returns false on failure
	 */
	public function sendReferralOrderPointsUpdateEmail($email_vars, $pts_for_order)
	{	
		extract($email_vars);
    // gather required information to compose email
		$settings = $this->getEmailSettings($customer_store_id);
		$content = $this->getEmailContent('referral_order_points_changes', $customer_store_id);
		$order = $this->getReferralOrder($customer_store_id, $order_id);

		if (!$this->isEmailConfigured($order, $settings, $content)) {
			// missing email settings
			return false;
		}
        
		$templateVariables = array(
			// customer information
			'%%CustomerFirstName%%' => $order['referrer_first_name'],
			'%%CustomerLastName%%' 	=> $order['referrer_last_name'],
			'%%CustomerEmail%%'		=> $order['referrer_email'],
			'%%OrderId%%'					=> $order['order_id'],
			'%%OrderPoints%%'		=> $pts_for_order,
			'%%PointsChangeReason%%' => $pts_update_notes,
		);

		// send email
		$result = Mail::send(
			$content['content'],
			$templateVariables,
			function(Message &$message) use ($content, $settings, $order) {
				//assign sender information
				
				$recipientName = $order['referrer_first_name'] . ' ' . $order['referrer_last_name'];
	
				$message
					->to($order['referrer_email'], $recipientName)
					->from($settings['email'], $settings['company_name'])
					->subject($content['subject']);

				if($content['bcc'] !== '' && $content['bcc'] !== null) {
					$message->bcc($content['bcc'], $content['bcc_name']);
				}
			}
		);
		
		return $result;
	}

	/**
	 * sends email to customer when there is an order has been completed in the store
	 *
	 * template tags available for this email:
	 *  - %%CustomerFirstName%%
	 *  - %%CustomerLastName%%
	 *  - %%CustomerEmail%%
	 *
	 * @param Subscription $subscription
	 * @return bool returns false on failure
	 */
	public function sendWelcomeEmail($email_vars)
	{	
		extract($email_vars);
		// gather required information to compose email
		$settings = $this->getEmailSettings($customer_store_id);
		$content = $this->getEmailContent('welcome', $customer_store_id);

		if (!$this->isEmailConfigured($order, $settings, $content)) {
			// missing email settings
			return false;
		}
        
		$templateVariables = array(
			// customer information
			'%%CustomerFirstName%%' => $first_name,
			'%%CustomerLastName%%' 	=> $last_name,
			'%%CustomerEmail%%'			=> $email,
		);

		// send email
		$result = Mail::send(
			$content['content'],
			$templateVariables,
			function(Message &$message) use ($content, $settings, $order) {
				//assign sender information
				$recipientName = $first_name . ' ' . $last_name;
	
				$message
					->to($order['email'], $recipientName)
					->from($settings['email'], $settings['company_name'])
					->subject($content['subject']);

				if($content['bcc'] !== '' && $content['bcc'] !== null) {
					$message->bcc($content['bcc'], $content['bcc_name']);
				}
			}
		);
		
		return $result;
	}

	/**
	 * returns the email settings for a customer store owner
	 *
	 * @param int $customer_store_id
	 * @return bool|array	returns false on failure
	 */
	public function getEmailSettings($customer_store_id)
	{
		try {
			/** @var MySQLConnection $db */
			$db = ConnectionManager::getInstance('mysql');

			$settings = $db->selectFirst('pts_settings', array('customer_store_id' => $customer_store_id));

			if (empty($settings)) return false;

			return $settings;
		} catch (ConnectionException $ce) {
			return false;
		} catch (UnknownConnectionTypeException $ue) {
			return false;
		}
	}

	/**
	 * returns the email subject and content for a recurring event
	 *
	 * @param string 		$eventName
	 * @param int 	$customer_store_id
	 * @return bool|array	returns false on failure
	 */
	public function getEmailContent($eventName, $customer_store_id)
	{
		try {
			/** @var MySQLConnection $db */
			$db = ConnectionManager::getInstance('mysql');
            
			$content = $db->selectFirst(
				'pts_email_content', array(
					'customer_store_id' => $customer_store_id, 'event_name' => $eventName
				)
			);
            
			if (empty($content)) return false;

			return $content;
		} catch (ConnectionException $ce) {
			return false;
		} catch (UnknownConnectionTypeException $ue) {
			return false;
		}
	}

	/**
	 * get a regular order from pts_orders table
	 *
	 * @param int $customer_store_id
	 * @param int $order_id
	 * @return bool|array returns false on failure, array on success
	 */
	private function getRegularOrder($customer_store_id, $order_id)
	{
		try {
			/** @var MySQLConnection $db */
			$db = ConnectionManager::getInstance('mysql');
			$order = $db->selectFirst('pts_orders',
				array(
					'customer_store_id' => $customer_store_id,
					'order_id'       => $order_id
				)
			);

			if (empty($order)) return false;

			return $order;
		} catch (ConnectionException $ce) {
			return false;
		} catch (UnknownConnectionTypeException $ue) {
			return false;
		}
	}

	/**
	 * get a referral order information from pts_orders and pts_customers table
	 *
	 * @param int $customer_store_id
	 * @param int $order_id
	 * @return bool|array returns false on failure, array on success
	 */
	private function getReferralOrder($customer_store_id, $order_id)
	{
		try {
			/** @var MySQLConnection $db */
			$db = ConnectionManager::getInstance('mysql');
			$order = $db->selectFirst('pts_referrals',
				array(
					'customer_store_id' => $customer_store_id,
					'order_id'       => $order_id
				)
			);

			if (empty($order)) return false;

			return $order;
		} catch (ConnectionException $ce) {
			return false;
		} catch (UnknownConnectionTypeException $ue) {
			return false;
		}
	}

	/**
	 * get the customer information
	 *
	 * @param int $customer_store_id
	 * @param int $bc_customer_id
	 * @return bool|array returns false on failure, array on success
	 */
	private function getCustomer($customer_store_id, $bc_customer_id)
	{
		try {
			/** @var MySQLConnection $db */
			$db = ConnectionManager::getInstance('mysql');
			$customer = $db->selectFirst('pts_customers',
				array(
					'customer_store_id' => $customer_store_id,
					'bc_customer_id'    => $bc_customer_id
				)
			);

			if (empty($customer)) return false;

			return $customer;
		} catch (ConnectionException $ce) {
			return false;
		} catch (UnknownConnectionTypeException $ue) {
			return false;
		}
	}
    
	/**
	 * check if the given variables contains all the information required to send email notification
	 *
	 * @param array $order		result from getOrder()
	 * @param array $settings	result from getEmailSettings()
	 * @param array $content	result from getEmailContent()
	 * @return bool
	 */
	private function isEmailConfigured($order, $settings, $content)
	{
		if (
			empty($order) || empty($settings) || empty($content)
			|| empty($settings['company_name']) || empty($settings['email'])
			|| empty($content['subject']) || empty($content['content'])
			|| (int)$content['enabled'] < 1
		) {
			// missing email settings
			return false;
		}

		return true;
	}

	private function formatPrice($price, $currency = '$')
	{
		return $currency . number_format((float)$price, 2);
	}

	/**
	 * returns an instance of this class
	 * @return EmailService
	 */
	public static function getInstance()
	{
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}

		return self::$instance;
	}
}