<?php
namespace MiniBC\addons\points\controllers;

use MiniBC\addons\points\entities\Product;
use MiniBC\addons\points\services\ProductService;

use MiniBC\addons\points\types\SubscriptionBox;
use MiniBC\core\Auth;
use MiniBC\core\EntityFactory;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\entities\Addon;
use MiniBC\core\entities\Store;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReportsController
{
	/** @var MySQLConnection $db */
    protected $db;

    /** @var Store $store */
    protected $store;

    /** @var Addon $addon */
    protected $addon;

	public function __construct()
	{
		$this->db = ConnectionManager::getInstance('mysql');
    $this->customer = Auth::getInstance()->getCustomer();
    $this->store = $this->customer->stores[0]; 
	}

	public function getDefaultChartDates($args = array())
	{
		$customer = Auth::getInstance()->getCustomer();
		$store = $customer->stores[0];
		$chartRangeStart = explode('-', date('Y-m-d') );
		$chartRangeStart = date( "Y-m-d", mktime(0,0,0 ,$chartRangeStart[1] - 1 ,$chartRangeStart[2],$chartRangeStart[0]) );	
		$chartRangeEnd = date('Y-m-d');
		$results = array(
			'chartRangeStart' => $chartRangeStart,
			'chartRangeEnd' => $chartRangeEnd,
		);
		header('Content-Type:text/json');
		echo json_encode($results);
		exit;
	}

	public function getChartData($chartType, Request $request)
	{
		$storeId = $this->customer->id;
		$maxDataPoints = 15;
		$dataPointInterval = 1;

		$chartRangeStart = $request->query->get('chartRangeStart', false);
		$chartRangeEnd = $request->query->get('chartRangeEnd', false);

		if (!isset($chartType) || empty($chartRangeStart) || empty($chartRangeEnd)) {
			return Response::create('', Response::HTTP_NOT_FOUND);
		}

		$startEpoch = strtotime($chartRangeStart);
		$endEpoch = strtotime($chartRangeEnd) + 86400;

		$startDate = \DateTime::createFromFormat('Y-m-d', $chartRangeStart);
		$endDate = \DateTime::createFromFormat('Y-m-d', $chartRangeEnd);
		$endDate = $endDate->modify('+2 day');
		$dateInterval = $endDate->diff($startDate);

		if ($dateInterval->days > $maxDataPoints) {
			$dataPointInterval = ceil($dateInterval->days / $maxDataPoints);
		}

		$datePeriod = new \DatePeriod($startDate, new \DateInterval('P' . $dataPointInterval . 'D'), $endDate);
		
		$regularPointsGiven = $this->calculateRegularPointsGiven($startEpoch,$endEpoch);
		$referralPointsGiven = $this->calculateReferralPointsGiven($startEpoch,$endEpoch);
		$ptsRedeemStoreCredit = $this->calculatePtsRedeemStoreCredit($startEpoch,$endEpoch);
		$ptsRedeemCoupon = $this->calculatePtsRedeemCoupon($startEpoch,$endEpoch);
		$totalPtsRedeem = $ptsRedeemCoupon['ptsRedeemCoupon'] + $ptsRedeemStoreCredit['ptsRedeemStoreCredit'];
		$totalPointsOut = $this->calculateTotalPointsOut();

		$dollarToPointsRatio = $this->db->queryFirst(
			"SELECT * FROM pts_settings WHERE customer_store_id = $storeId"
		);

		$totalDollarAmt = $totalPointsOut["totalPointsOut"] / $dollarToPointsRatio['dollar_pts_ratio'];

		//////Formatting/////
		// setlocale(LC_MONETARY, 'en_US.UTF-8');
		// $totalRevenue = money_format('%.2n', $totalRevenue);	

		////PREPPING CHART DATA:
		$chartRangeEndDayExploded = explode('-',$chartRangeStart);
		$chartRangeEndDayExploded[2]+=1;
		$chartRangeEndSumExploded = explode('-',$chartRangeEnd);

		$labelArray = [];
		$dataArray = [];

		if ($chartType == 'regularPointsGiven') {
			$results = $this->db->query(
				'SELECT * FROM pts_orders WHERE create_time >= :startEpoch AND create_time <= :endEpoch AND customer_store_id = :customer_id',
				array(
					':customer_id' => $storeId,
					':startEpoch' => (int)$startDate->format('U'),
					':endEpoch' => (int)$endDate->format('U')
				)
			);

			$lastDate = $startDate;

			foreach ($datePeriod as $date) {

				/** @var \DateTime $date */
				$chartRangeStartDayEpoch = (int)$lastDate->format('U');
				$chartRangeEndDayEpoch = (int)$date->format('U');

				array_push($labelArray, $date->format('m/d'));
				$total = 0;

				foreach ($results as $result) {
					if (
						$result['create_time'] >= $chartRangeStartDayEpoch
						&& $result['create_time'] <= $chartRangeEndDayEpoch
					) {
						$total += $result['points_received'];
					}
				}

				array_push($dataArray, $total);

				$lastDate = $date;
			}
		} else if ($chartType=='referralPointsGiven') {
			$results = $this->db->query(
				'SELECT * from pts_referrals WHERE create_time>=:startEpoch AND create_time<=:endEpoch AND customer_store_id = :customer_id',
				array(
					':customer_id' => $storeId,
					':startEpoch' => $startEpoch,
					':endEpoch' => $endEpoch
				)
			);

			$lastDate = $startDate;

			foreach ($datePeriod as $date) {
				/** @var \DateTime $date */
				$chartRangeStartDayEpoch = (int)$lastDate->format('U');
				$chartRangeEndDayEpoch = (int)$date->format('U');

				array_push($labelArray, $date->format('m/d'));
				$total = 0;

				foreach ($results as $result) {
					if (
						$result['create_time'] >= $chartRangeStartDayEpoch
						&& $result['create_time'] <= $chartRangeEndDayEpoch
					) {
						$total += $result['points_reward'];
					}
				}

				array_push($dataArray, $total);

				$lastDate = $date;
			}
		}
	
		$results = array(
			'chartRangeStart' => $chartRangeStart,
			'chartRangeEnd' => $chartRangeEnd,
			'chartType' => $chartType,
			'label_array' => $labelArray,
			'data_array' => $dataArray,
			'total_dollar_amt' => $totalDollarAmt,
			'total_points_out' => $totalPointsOut,
			'total_points_redeem' => $totalPtsRedeem,
			'pts_redeem_store_credit' => $ptsRedeemStoreCredit,
			'pts_redeem_coupon'    => $ptsRedeemCoupon,
			'regular_points_given' 		=> $regularPointsGiven,
			'referral_points_given' 	=> $referralPointsGiven,

		);

		return JsonResponse::create($results);
	}

	/**
	* Calculate the total outstanding points that has no been redeem for liability reason of the store
	*
	*
	*/

  public function calculateTotalPointsOut()
  {
  	$totalPointsOut = $this->db->queryFirst(
  		'
  		SELECT SUM( c.`points_remaining` ) AS totalPointsOut
  		FROM `pts_customers` c
  		WHERE 
  			c.`customer_store_id` = :customer_store_id
  		',
  		array(
  			':customer_store_id' => $this->customer->id
  		)
  	);

  	return $totalPointsOut;
  }

	/**
	* Calculate the points whihc are given based on the regular purchase order on a given periond that has no been 
	* redeem for liability reason of the store
	*
	*	@param - $startEpoch: UNIX unit time starting time point
	* @param - $endEpoch: UNIX unit time ending time point
	* @return - $regularPointsGiven
	*/

  public function calculateRegularPointsGiven($startEpoch, $endEpoch)
  {
  	$regularPointsGiven = $this->db->queryFirst(
  		'
  		SELECT SUM( o.`points_received` ) AS regularPointsGiven
  		FROM `pts_orders` o
  		WHERE 
  			o.`customer_store_id` = :customer_store_id
  			AND `create_time` >= :start_time
  			AND `create_time`  <= :end_time
   		',
   		array(
   			":customer_store_id" 	=> $this->customer->id,
   			"start_time"					=> $startEpoch,
   			"end_time"						=> $endEpoch
   		)
  	);

  	return $regularPointsGiven;
  }

  /**
	* Calculate the points whihc are given when a referral get involved for an order on a given periond that has no * been redeem for liability reason of the store
	*
	*	@param - $startEpoch: UNIX unit time starting time point
	* @param - $endEpoch: UNIX unit time ending time point
	* @return - $referralPointsGiven
	*/

  public function calculateReferralPointsGiven($startEpoch, $endEpoch)
  {
  		$referralPointsGiven = $this->db->queryFirst(
  		'
  		SELECT SUM( r.`points_reward` ) AS referralPointsGiven
  		FROM `pts_referrals` r
  		WHERE 
  			r.`customer_store_id` = :customer_store_id
  			AND `create_time` >= :start_time
  			AND `create_time`  <= :end_time
   		',
   		array(
   			":customer_store_id" 	=> $this->customer->id,
   			"start_time"					=> $startEpoch,
   			"end_time"						=> $endEpoch
   		)
  	);

  	return $referralPointsGiven;
  }

  public function calculatePtsRedeemStoreCredit($startEpoch, $endEpoch)
  {
  		$ptsRedeemStoreCredit = $this->db->queryFirst(
  		'
  		SELECT SUM( r.`pts_redeem_store_credit` ) AS ptsRedeemStoreCredit
  		FROM `pts_customers` r
  		WHERE 
  			r.`customer_store_id` = :customer_store_id
  			AND `create_time` >= :start_time
  			AND `create_time`  <= :end_time
   		',
   		array(
   			":customer_store_id" 	=> $this->customer->id,
   			"start_time"					=> $startEpoch,
   			"end_time"						=> $endEpoch
   		)
  	);

  	return $ptsRedeemStoreCredit;
  }

  public function calculatePtsRedeemCoupon($startEpoch, $endEpoch)
  {
  		$ptsRedeemCoupon = $this->db->queryFirst(
  		'
  		SELECT SUM( r.`pts_redeem_coupon` ) AS ptsRedeemCoupon
  		FROM `pts_customers` r
  		WHERE 
  			r.`customer_store_id` = :customer_store_id
  			AND `create_time` >= :start_time
  			AND `create_time`  <= :end_time
   		',
   		array(
   			":customer_store_id" 	=> $this->customer->id,
   			"start_time"					=> $startEpoch,
   			"end_time"						=> $endEpoch
   		)
  	);

  	return $ptsRedeemCoupon;
  }


}




