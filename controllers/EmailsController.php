<?php
namespace MiniBC\addons\points\controllers;

use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\Auth;
use MiniBC\core\entities\Store;
use MiniBC\core\exceptions\NotFoundException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EmailsController
{
	protected $events = array();

	/** @var Store $store */
	protected $store;

	public function __construct()
	{	

		$this->db = ConnectionManager::getInstance('mysql');
		$this->events = array(
			'welcome' => array(
				'label' => 'Welcome',
				'desc'	=> 'This email will be sent to the customer when they first time use this stystem.',
				'template_vars' => 'points-email-vars-welcome'
			),
			'regular_order_points_changes' => array(
				'label' => 'Regular Order Points Chages',
				'desc'	=> 'This email will be sent to the customer when there is a changes on their regular order points.',
				'template_vars' => 'points-email-vars-regular-order-points-update'
			),
			'referral_order_points_changes' => array(
				'label' => 'Referral Order Points Chages',
				'desc'	=> 'This email will be sent to the customer when there is a changes on their referral order points.',
				'template_vars' => 'points-email-vars-referral-order-points-update'
			),
		);

		$this->customer = Auth::getInstance()->getCustomer();
	}

	/**
	 * assign current store
	 *
	 * @param Store $store
	 */
	public function setStore(Store $store)
	{
		$this->store = $store;
	}

	public function getEmails()
	{
		$emailEvents = array();

		foreach ($this->events as $event => $event_object) {

			try {
				$emailEvents[] = $this->getEmailData($event, $event_object);
			} catch (NotFoundException $nf) {
				$emailEvents[] = $this->getDefaultEvent($event);
			}
		}

		return JsonResponse::create(array( 'points-emails' => $emailEvents ));
	}

	public function getEmail($event)
	{
		try {
			$content = $this->getEmailData($event);
		} catch (NotFoundException $nf) {
			$content = $this->getDefaultEvent($event);
		}

		return JsonResponse::create(array( 'points-email' => $content ));
	}

	public function updateEmail($event, Request $request)
	{
		
		$customer_store_id = $this->customer->id;
		$emailData = $request->request->get('pointsEmail', false);

		if (empty($emailData)) {
			return Response::create('', Response::HTTP_BAD_REQUEST);
		}

		$now = time();

		$dbData = array(
			'subject' => $emailData['subject'],
			'bcc' => $emailData['bcc'],
			'bcc_name' => $emailData['bccName'],
			'enabled' => ($emailData['enabled'] === true),
			'content' => $emailData['content'],
			'update_time' => $now
		);
		
		$whereParams = array( 'event_name' => $event['id'], 'customer_store_id' => $customer_store_id );
	
		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');

		try {
			$this->getEmailData($event['id']);
			$db->update('pts_email_content', $dbData, $whereParams);
		} catch (NotFoundException $nf) {
			$dbData = array_merge($dbData, $whereParams);
			$dbData['create_time'] = $now;

			$db->insert('pts_email_content', $dbData);
		}

		return $this->getEmail($event['id']);
	}


	protected function getEmailData($event)
	{
		// var_dump($event);
		$customer_store_id = $this->customer->id;
		/** @var MySQLConnection $db */
		
		$content = $this->db->selectFirst(
			'pts_email_content',
			array( 'customer_store_id' => $customer_store_id, 'event_name' => $event )
		);
	
		if (!empty($content)) {
			return array(
				'id' => $event,
				'name' => $this->events[$event]['label'],
				'desc' => $this->events[$event]['desc'],
				'subject' => $content['subject'],
				'bcc' => $content['bcc'],
				'bccName' => $content['bcc_name'],
				'enabled' => ((int)$content['enabled'] > 0),
				'content' => $content['content'],
				'configured' => true,
				'update_time' => (int)$content['update_time'],
				'variables' => $this->events[$event]['template_vars']
			);
		}

		throw new NotFoundException('Email template not found for "' . $event . '"');
	}

	/**
	 * returns default values for an email event
	 *
	 * @param string $event
	 * @return array
	 */

	private function getDefaultEvent($event)
	{
		return array(
			'id' => $event,
			'name' => $this->events[$event]['label'],
			'desc' => $this->events[$event]['desc'],
			'subject' => '',
			'enabled' => 0,
			'content' => '',
			'configured' => false,
			'update_time' => 0,
			'variables' => $this->events[$event]['template_vars']
		);
	}
}