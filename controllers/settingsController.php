<?php
namespace MiniBC\addons\points\controllers;

use \DateTime;
use Bigcommerce\Api\Client;
use MiniBC\core\Auth;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\entities\Addon;
use MiniBC\core\Mail;
use MiniBC\core\mail\Message;
use MiniBC\core\EntityFactory;
use MiniBC\bigcommerce\services\StorefrontAssetsService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use MiniBC\core\services\exception\WebDAVConnectionFailedException;

class SettingsController
{
    private $db = null;
    private $addon = null;
    private $categories = null;
    private $limit = null;
    private $customer = null;
    private $store = null;
    private $apiConnection = null;
    private $email_template_path;
    private $storecolor;
    private $webDavService = false;

    public function __construct()
    {   
        $this->db = ConnectionManager::getInstance('mysql');
        $this->customer = Auth::getInstance()->getCustomer();
        $this->store = $this->customer->stores[0];  
    }

    public function getSettings()
    {   

        $customer_store_id = $this->customer->id;

        $query = "SELECT * FROM pts_settings WHERE customer_store_id = $customer_store_id";
        $settings = $this->db->queryFirst($query);
        $result["pointsSettings"] = $settings;
        echo json_encode($result);    
        exit();
    }

    public function updateSettings($id, Request $request)
    {
        $data = $request->request->get('pointsSetting', false);
        $dbUpdateRes = $this->db->update('pts_settings', $data, array('id' => $id));

        if($dbUpdateRes) {
            $res['success'] = true;
            return JsonResponse::create($res);
        }

        return Response::create('', Response::HTTP_INTERNAL_SERVER_ERROR);
    }

}