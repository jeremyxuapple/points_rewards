<?php
namespace MiniBC\addons\points\controllers;

use \DateTime;
use Bigcommerce\Api\Client;
use MiniBC\core\Auth;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\entities\Addon;
use MiniBC\core\Mail;
use MiniBC\core\mail\Message;
use MiniBC\core\EntityFactory;
use MiniBC\bigcommerce\services\StorefrontAssetsService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use MiniBC\core\services\exception\WebDAVConnectionFailedException;
use MiniBC\addons\points\services\EmailService;

class CustomersController
{
	  private $db = null;
    private $addon = null;
    private $categories = null;
    private $limit = null;
    private $customer = null;
    private $store = null;
    private $apiConnection = null;
    private $email_template_path;
    private $storecolor;
    private $webDavService = false;

    public function __construct()
    {   
      $this->db = ConnectionManager::getInstance('mysql');
      $this->customer = Auth::getInstance()->getCustomer();
      $this->store = $this->customer->stores[0];  
      $this->emailService = EmailService::getInstance();
    }

    public function getRedeemRecords()
    {	
    	$customer_store_id = $this->customer->id;
    	$records = $this->db->query('
    		SELECT r.`id`, r.`redemption_option`, r.`pts_redeemed`, r.`dollar_value`, r.`coupon_code`, FROM_UNIXTIME(r.`create_time`) AS create_time, c.`first_name`, c.`last_name`, c.`email`
    		FROM pts_redemption_records r
    		LEFT JOIN pts_customers c
    		ON r.`bc_customer_id` = c.`bc_customer_id`
    		WHERE r.`customer_store_id` = :customer_store_id
        ORDER BY r.`create_time` DESC 
    	 	',
        array(
          ":customer_store_id" => $customer_store_id,
        )
      );

    	$result["pointsRedeems"] = $records;
      header('Content-Type: text/json');
      echo json_encode($result);
      exit();
    }
}




