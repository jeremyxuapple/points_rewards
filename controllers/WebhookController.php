<?php
namespace MiniBC\addons\points\controllers;

use \DateTime;
use Bigcommerce\Api\Client;
use MiniBC\bigcommerce\services\StorefrontAssetsService;

use MiniBC\core\Config;
use MiniBC\core\Auth;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\entities\Addon;
use MiniBC\core\Mail;
use MiniBC\core\mail\Message;
use MiniBC\core\EntityFactory;
use MiniBC\core\services\exception\WebDAVConnectionFailedException;
use Firebase\JWT\JWT;

use MiniBC\addons\points\services\MandrillEmailService;

class WebhookController
{
	private $db = null;
  private $addon = null;
  private $categories = null;
  private $limit = null;
  private $customer = null;
  private $store = null;
  private $apiConnection = null;
  private $email_template_path;
  private $storecolor;
  private $webDavService = false;

  public function __construct()
  {   
    $this->db = ConnectionManager::getInstance('mysql');
    $this->customer = Auth::getInstance()->getCustomer();
    $this->store = $this->customer->stores[0];  
    $this->mandrillEmailService = MandrillEmailService::getInstance();
  }

  /**
  * Create the webhook for the store, only need to call once. Or try to call the endpoint first to check 
  * if the webhook have already been installed.
  */
  public function createWebhook()
  {   
      $api = $this->store->getApiConnection(); 

      // // Create product option set
      // $api::createOptionsets(array( 'name' => 'referrer_code' ));

      // // Create product options
      // $api::createOptions(array(
      //   "name" => "referrer_code",
      //   "type" => "T"
      // ));

      // // Grab Option Set Id
      // $optionSets = $api::getOptionSets();
      // foreach ($optionSets as $optionSet) {
      //   if( $optionSet->name == 'referrer_code' ) {
      //     $optionSetId = $optionSet->id;
      //   }
      //   break;
      // }

      // // Grab the optionId
      // $options = $api::getOptionSets();
      // foreach ($options as $option) {
      //   if( $option->name == 'referrer_code' ) {
      //     $optionId = $option->id;
      //   }
      //   break;
      // }

      // // Connection option set with the option
      // $api::createOptionsets_Options( array( "option_id" => $optionId ), $optionSetId);

      // generate the token
      $payload = array( 'store_id' => $this->customer->id );
      $storeToken = JWT::encode($payload, Config::get('site::secret_key'), 'HS512');

      $webhook_order = array(
            'scope' => 'store/order/statusUpdated',
            'headers' => array(
                'x-store-token' => $storeToken
              ),
            'destination' => 'https://staging.minibc.com/customer/apps/points/createPtsOrder',
            'is_active' => true
        );

       // $webhook_customer = array(
       //      'scope' => 'store/customer/created',
       //      'headers' => array(
       //          'x-store-token' => $storeToken
       //        ),
       //      'destination' => 'https://staging.minibc.com/customer/apps/points/createPtsCustomer',
       //      'is_active' => true
       //  );

       // $webhook_product = array(
       //      'scope' => 'store/product/created',
       //      'headers' => array(
       //          'x-store-token' => $storeToken
       //        ),
       //      'destination' => 'https://staging.minibc.com/apps/points/updateProductOption', 
       //      'is_active' => true
       //  );

       $api::createWebhook($webhook_order);
       // $api::createWebhook($webhook_customer);
       // $api::createWebhook($webhook_product);

      //Check if the webhook have been created
      // print_r($api::getWebhooks());
      // exit();

  }

  /**
  *
  * Listen to the Big Commerce customer creation webhook call and create a profile for the customer in the 
  * points system
  * 
  */

  public function createPtsCustomer()
  { 
    // Grab the data from Big Commerce webhook
    $webhookContent = file_get_contents("php://input");
    $webhooks = json_decode($webhookContent, true);
    http_response_code(200);

    $bc_customer_id = $webhooks["data"]["id"]; 

    $api = $this->store->getApiConnection(); 
    $customer = $api::getCustomer($bc_customer_id);

    $pts_customer = array(
      'customer_store_id' => $this->customer->id,
      'bc_customer_id'    => $bc_customer_id,
      'points_remaining'  => 0,
      'first_name'        => $customer->first_name,
      'last_name'         => $customer->last_name,
      'email'             => $customer->email,
      'referral_code'     => $this->generateRandomString(),
      'create_time'       => time(),
      'update_time'       => time()
    );

    $this->db->insert("pts_customers", $pts_customer);
  }

  /**
   * Generates a random string
   *
   * @param $length - the length of the desired string
   *
   * @return $randomString - the random string
   *
   */

    public function generateRandomString($length = 6)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

  /**
  *
  * Listen to the Big Commerce product creation webhook call and update the product with options in the 
  * points system
  * 
  */

   public function updateProductOptionSet()
   {  
      $api = $this->store->getApiConnection(); 
      
      // Grab the data from Big Commerce webhook
      $webhookContent = file_get_contents("php://input");
      $webhooks = json_decode($webhookContent, true);

      http_response_code(200);

      $product_id = $webhooks["data"]["id"]; 

      $optionSets = $api::getOptionSets();
    
      foreach ($optionSets as $optionSet) {
        if( $optionSet->name == 'referrer_code' ) {
          $optionSetId = $optionSet->id;
        }
        break;
      }
      
      $api::updateProduct($product_id, array(
        'option_set_id' => $optionSetId
      ));

      print_r($api::getLastError());
   }

}