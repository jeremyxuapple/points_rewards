<?php
namespace MiniBC\addons\points\controllers;

use \DateTime;
use Bigcommerce\Api\Client;
use MiniBC\core\Auth;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\entities\Addon;
use MiniBC\core\Mail;
use MiniBC\core\mail\Message;
use MiniBC\core\EntityFactory;
use MiniBC\bigcommerce\services\StorefrontAssetsService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use MiniBC\core\services\exception\WebDAVConnectionFailedException;
use MiniBC\addons\points\services\EmailService;

class ReferralController
{
    private $db = null;
    private $addon = null;
    private $categories = null;
    private $limit = null;
    private $customer = null;
    private $store = null;
    private $apiConnection = null;
    private $email_template_path;
    private $storecolor;
    private $webDavService = false;

    public function __construct()
    {   
        $this->db = ConnectionManager::getInstance('mysql');
        $this->customer = Auth::getInstance()->getCustomer();
        $this->store = $this->customer->stores[0];  
        $this->emailService = EmailService::getInstance();
    }
    
    /**
    * Get order list that are completed based on a referral
    */
    public function getReferralOrders($id = 0)
    {
        $customer_store_id = $this->customer->id;

        if (isset($_GET['term'])) {
            // When there is a search term passed in
            $term = $_GET['term'];

            $referral_orders = $this->db->query("
                SELECT * FROM pts_referrals 
                WHERE customer_store_id = $customer_store_id
                AND order_id LIKE '%$term%'
                OR referrer_first_name LIKE '%$term%'
                OR referrer_last_name LIKE '%$term%'
                OR referrer_email LIKE '%$term%'
                OR referee_first_name LIKE '%$term%'
                OR referee_last_name LIKE '%$term%'
                OR referee_email LIKE '%$term%'
                ");

            $result["pointsReferrals"] = $referral_orders;
            header('Content-Type: text/json');
            echo json_encode($result);
            exit();

        } else {

                // When the page first load and there is no search term passed in
                $referral_orders = $this->db->query("SELECT * FROM pts_referrals WHERE customer_store_id = $customer_store_id");
                
                 if(empty($id)) {  
                    $result["pointsReferrals"] = $referral_orders;
                    header('Content-Type: text/json');
                    echo json_encode($result);
                    exit();
                } else { 
                    foreach ($referral_orders as $referral_order) {
                        if($referral_order["id"] == $id) {
                            $result["pointsReferral"] = $referral_order;
                            header('Content-Type: text/json');
                            echo json_encode($result);
                            exit();
                        }
                    }
                } // end of single referral order search

            } // end of the else search term
    }

    public function updateReferralOrders($id, Request $request)
    {
        $data = $request->request->get('pointsReferral', false);

        $update = array(
            'points_reward' => $data["points_reward"],
            'pts_update_notes' => $data["pts_update_notes"]
        );
        $dbUpdateRes = $this->db->update("pts_referrals", $update, array('id' => $id));
        
        // Let's send out email template

        $email_vars = array(
            'customer_store_id' => $data['customer_store_id'],
            'order_id'          => $data['order_id'],
            'pts_update_notes'  => $data['pts_update_notes'],
        );

        $this->emailService->sendReferralOrderPointsUpdateEmail($email_vars, $data["points_reward"]);
        if($dbUpdateRes) {
            $res['success'] = true;
            return JsonResponse::create($res);
        }

        return Response::create('', Response::HTTP_INTERNAL_SERVER_ERROR);
    }


}