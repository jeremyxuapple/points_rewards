<?php
namespace MiniBC\addons\points\controllers;

use \DateTime;
use Bigcommerce\Api\Client;
use MiniBC\core\Auth;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\entities\Addon;
use MiniBC\core\Mail;
use MiniBC\core\mail\Message;
use MiniBC\core\EntityFactory;
use MiniBC\bigcommerce\services\StorefrontAssetsService;
use MiniBC\core\services\exception\WebDAVConnectionFailedException;
use MiniBC\addons\points\services\MandrillEmailService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class StorefrontController
{
	private $db = null;
  private $addon = null;
  private $categories = null;
  private $limit = null;
  private $customer = null;
  private $store = null;
  private $apiConnection = null;
  private $email_template_path;
  private $storecolor;
  private $webDavService = false;

  public function __construct()
  {   
    $this->db = ConnectionManager::getInstance('mysql');
    $this->customer = Auth::getInstance()->getCustomer();
    $this->store = $this->customer->stores[0];  
    $this->mandrillEmailService = MandrillEmailService::getInstance();
  }


  /**
  *
  * Populate the login customer information when customer land on the points program page
  *
  */

  public function getCustomerInfo()
  {
  	$bc_customer_id = $_GET['customer_id'];
  	$customer_store_id = $this->customer->id;

  	$customer = $this->db->queryFirst(
  		'SELECT * FROM pts_customers WHERE bc_customer_id = :customer_id AND customer_store_id = :customer_store_id',
  		array(
  			':customer_id' => $bc_customer_id,
  			':customer_store_id' => $customer_store_id
  		)
  	);

    $redeemRatio = $this->db->queryFirst(
      'SELECT * FROM pts_settings WHERE customer_store_id = :customer_store_id', 
      array(
        'customer_store_id' => $customer_store_id
      )
    );

    // Get all the order information that belogs to this customer
    $orders = $this->db->query(
      'SELECT *, FROM_UNIXTIME(`create_time`) AS create_time 
       FROM pts_orders
       WHERE customer_store_id = :customer_store_id
       AND bc_customer_id = :bc_customer_id',
       array(
        ':bc_customer_id' => $bc_customer_id,
        ':customer_store_id' => $customer_store_id
      )
    );

    // Get all the referral order information that belogs to this customer
    $referralRecords= $this->db->query(
      'SELECT *, FROM_UNIXTIME(`create_time`) AS create_time 
       FROM pts_referrals
       WHERE customer_store_id = :customer_store_id
       AND referrer_customer_id = :referrer_customer_id',
       array(
        ':referrer_customer_id' => $bc_customer_id,
        ':customer_store_id' => $customer_store_id
      )
    );

    // Get all the redeemption records information that belogs to this customer
    $redemptionRecords = $this->db->query(
      'SELECT *, FROM_UNIXTIME(`create_time`) AS create_time 
       FROM pts_redemption_records
       WHERE customer_store_id = :customer_store_id
       AND bc_customer_id = :bc_customer_id',
       array(
        ':bc_customer_id' => $bc_customer_id,
        ':customer_store_id' => $customer_store_id
      )
    );

    $customer["orders"] = $orders;
    $customer["referralRecords"] = $referralRecords;
    $customer["redemptionRecords"] = $redemptionRecords;
    $customer["redeem_ratio"] = $redeemRatio['dollar_pts_ratio'];

    echo json_encode($customer);
  }

  /**
  *
  * Receive the points of how much the customer wish to redeem from the storefront and redeem as store credit
  *
  */

  public function redeemToStoreCredit()
  {	
  	$api = $this->store->getApiConnection(); 
    $customer_store_id = $this->customer->id;

  	$pts_to_redeem = $_POST['pts_to_redeem'];
    $bc_customer_id = $_POST['bc_customer_id'];

// let's update the points of this customer

    $customer_info = $this->db->queryFirst("
        SELECT * FROM pts_customers 
        WHERE customer_store_id = $customer_store_id 
        AND bc_customer_id = $bc_customer_id"
    );

    $points_update = $customer_info["points_remaining"] - $pts_to_redeem;
    $pts_redeem_store_credit = $customer_info['pts_redeem_store_credit'] + $pts_to_redeem;
    if($points_update < 0) {
      exit("Insufficient points, please check you have enough points!");
    }

    $this->db->update("pts_customers", 
      array(
        "points_remaining" => $points_update,
        "pts_redeem_store_credit" => $pts_redeem_store_credit
      ), 
      array(
        'customer_store_id' => $customer_store_id, 
        'bc_customer_id' => $bc_customer_id
      )
    );

// let's create the store credit for this customer

    $query = "SELECT * FROM pts_settings WHERE customer_store_id = $customer_store_id";
    $settings = $this->db->queryFirst($query);

    $ratio = $settings['dollar_pts_ratio'];
    $dollar_amt = (int)$pts_to_redeem / (int)$ratio;

    $customer = $api::getCustomer($bc_customer_id);
   
    $updated_credit = $customer->store_credit + $dollar_amt;

    $store_credit = (object)array('store_credit' => $updated_credit);

    $api->updateCustomer($bc_customer_id, $store_credit);

    $error = $api->getLastError();
    print_r($error);

// Let's insert a new row into redeem records table

    $redemptionRecord = array(
      'customer_store_id' => $customer_store_id,
      'bc_customer_id' => $bc_customer_id, 
      'redemption_option' => "store credit",
      'pts_redeemed' => $pts_to_redeem,
      'dollar_value' => $dollar_amt,
      'create_time' => time(),
    );

    $this->db->insert("pts_redemption_records", $redemptionRecord);

    $response = array(
      'success' => true,
      'dollar_amt' => $dollar_amt
    );

    echo json_encode($response);
  }
  
  /**
  *
  * Receive the points of how much the customer wish to redeem from the storefront and redeem as coupon and an 
  * email will be generated for the customer's record
  *
  */ 

  public function redeemToCoupon()
  {	
  	$api = $this->store->getApiConnection(); 
    $customer_store_id = $this->customer->id;

    $pts_to_redeem = $_POST["pts_to_redeem"];
    $bc_customer_id = $_POST["bc_customer_id"];

    // let's update the points of this customer

    $customer_info = $this->db->queryFirst("SELECT * FROM pts_customers WHERE customer_store_id = $customer_store_id AND bc_customer_id = $bc_customer_id");

    $points_update = $customer_info["points_remaining"] - $pts_to_redeem;
    $pts_redeem_coupon = $customer_info['pts_redeem_coupon'] + $pts_to_redeem; 

    if($points_update < 0) {
      exit("Insufficient points, please check you have enough points!");
    }

    $this->db->update("pts_customers", 
      array(
        "points_remaining" => $points_update,
        "pts_redeem_coupon" => $pts_redeem_coupon
      ), 
      array(
        'customer_store_id' => $customer_store_id, 
        'bc_customer_id' => $bc_customer_id
      )
    );

    $query = "SELECT * FROM pts_settings WHERE customer_store_id = $customer_store_id";
    $settings = $this->db->queryFirst($query);

    $ratio = $settings['dollar_pts_ratio'];
    $dollar_amt = (int)$pts_to_redeem / (int)$ratio;
    $code = $this->generateRandomString(13);
    $coupon = (object)array(
    	'name' => 'customer_id= ' . $bc_customer_id . ' ' .  $code,
      'code' => $code,
      'applies_to' => array(
          "entity" => "categories",
          "ids" => [0]
        ),
    	'type' => 'per_total_discount',
    	'enabled' => true,
    	'amount' => $dollar_amt,
      'max_uses' => 1,
      'max_uses_per_customer' => 1
    );

    $api::createCoupon($coupon);
    $error = $api->getLastError();
    print_r($error);

    $template_content = array(
      array(
        'name' => 'coupon_code',
        'content' => $code
      ),
    );
    // Need to generate an email include the coupon code for the customer records
    $this->mandrillEmailService->sendMailTemplate("jeremy@beapartof.com", "jeremy@beapartof.com", "Coupon Codes", "Points_coupon", $template_content);


    // Let's insert a new row into redeem records table

    $redemptionRecord = array(
      'customer_store_id' => $customer_store_id,
      'bc_customer_id' => $bc_customer_id, 
      'redemption_option' => "coupon",
      'coupon_code' => $code,
      'pts_redeemed' => $pts_to_redeem,
      'dollar_value' => $dollar_amt,
      'create_time' => time(),
    );

    $this->db->insert("pts_redemption_records", $redemptionRecord);

    $response = array(
      'success' => true,
      'coupon_code' => $code,
      'dollar_amt' => $dollar_amt
    );

    echo json_encode($response);
  }

  /**
 * get default stylesheet for account area
 *
 * @param Request $request
 */
  public function getDefaultStylesheet(Request $request)
  {
    // $customer = Auth::getInstance()->getCustomer();
   
    $file = 'styles.css';

    // $isStencil = (bool)$request->request->get('stencil', false);

    // if ($isStencil) {
    //   $file = 'stencil.styles.css';
    // }

    $path = realpath(dirname(__FILE__) . '/../storefront/css/' . $file);
    // $customerPath = realpath(dirname(__FILE__) . '/../storefront/css/' . $customer->id . '/' . $file);

    $isDefaultPath = file_exists($path);
    // $isCustomerPath = file_exists($customerPath);

    if (!$isDefaultPath) {
      Response::create('', Response::HTTP_NOT_FOUND)->send();
      exit;
    }

    // $filePath = ($isCustomerPath) ? $customerPath : $path;
    $filePath = $path;

    Response::create(file_get_contents($filePath), Response::HTTP_OK, array( 'Content-Type' => 'text/css' ))->send();

    exit;
  }

/**
 * Generates a random string
 *
 * @param $length - the length of the desired string
 *
 * @return $randomString - the random string
 *
 */

  public function generateRandomString($length = 6)
  {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
  }

}