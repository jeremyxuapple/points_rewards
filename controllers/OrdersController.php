<?php
namespace MiniBC\addons\points\controllers;

use \DateTime;
use Bigcommerce\Api\Client;
use MiniBC\core\Auth;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\entities\Addon;
use MiniBC\core\Mail;
use MiniBC\core\mail\Message;
use MiniBC\core\EntityFactory;
use MiniBC\bigcommerce\services\StorefrontAssetsService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use MiniBC\core\services\exception\WebDAVConnectionFailedException;
use MiniBC\addons\points\services\EmailService;

class OrdersController
{
    private $db = null;
    private $addon = null;
    private $categories = null;
    private $limit = null;
    private $customer = null;
    private $store = null;
    private $apiConnection = null;
    private $email_template_path;
    private $storecolor;
    private $webDavService = false;

    public function __construct()
    {   
        $this->db = ConnectionManager::getInstance('mysql');
        $this->customer = Auth::getInstance()->getCustomer();
        $this->store = $this->customer->stores[0];  
        $this->emailService = EmailService::getInstance();
    }

    public function getOrders($id = 0)
    {     
        $customer_store_id = $this->customer->id;

        if (isset($_GET['term'])) {
            
            $term = $_GET['term'];
            // When there is a search term passed in, we will make a new query base on the term
            $orders_query = "
            SELECT o.*, c.first_name, c.last_name, c.email, c.points_remaining 
            FROM pts_orders o 
            LEFT JOIN pts_customers c 
            ON o.customer_store_id = c.customer_store_id 
            AND o.bc_customer_id = c.bc_customer_id
            WHERE o.customer_store_id = $customer_store_id
            AND c.first_name LIKE '%$term%'
            OR c.last_name LIKE '%$term%'
            OR c.email LIKE '%$term%'
            OR o.order_id LIKE '%$term%'
            ";
                
            $orders = $this->db->query($orders_query);

            $result["pointsOrders"] = $orders;
            header('Content-Type: text/json');
            echo json_encode($result);
            exit();
        
        } else {
            
            // When the page first load, no terms passed in

                $orders_query = "SELECT o.*, c.first_name, c.last_name, c.email, c.points_remaining FROM pts_orders o LEFT JOIN pts_customers c ON o.customer_store_id = c.customer_store_id 
                AND o.bc_customer_id = c.bc_customer_id
                WHERE o.customer_store_id = $customer_store_id";
                
                $orders = $this->db->query($orders_query);
               
                if(empty($id)) {  
                    $result["pointsOrders"] = $orders;
                    header('Content-Type: text/json');
                    echo json_encode($result);
                    exit();
                } else { 
                    foreach ($orders as $order) {
                        if($order["id"] == $id) {
                            $result["pointsOrder"] = $order;
                            header('Content-Type: text/json');
                            echo json_encode($result);
                            exit();
                        }
                    }
                }
            }
        

        
    }

    /**
     * update order
     *
     * @param int $id points order id
     * @param Request $request HTTP response
     * @return JsonResponse|Response
     */

    public function updateOrder($id, Request $request)
    {   
        $data = $request->request->get('pointsOrder', false);
        $order_update = array(
            "points_received" => $data["points_received"],
            'pts_update_notes' => $data['pts_update_notes'],
        );
        $this->db->update("pts_orders", $order_update, array('id' => $id));

        $where = array(
            'customer_store_id' => $data['customer_store_id'],
            'bc_customer_id'    => $data['bc_customer_id']
        );

        $customer_update = array(
            'points_remaining' => $data["points_remaining"],
        );
        $dbUpdateRes = $this->db->update("pts_customers", $customer_update, $where);

        // Let's send out email template

        $email_vars = array(
            'customer_store_id' => $data['customer_store_id'],
            'order_id'          => $data['order_id'],
            'pts_update_notes'  => $data['pts_update_notes'],
        );
        
        $this->emailService->sendRegularOrderPointsUpdateEmail($email_vars, $data["points_received"]);

        if($dbUpdateRes) {
            $res['success'] = true;
            return JsonResponse::create($res);
        }

        return Response::create('', Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function createPtsOrder()
    {   
        $api = $this->store->getApiConnection(); 
        $customer_store_id = $this->customer->id;

       // Grab the data from Big Commerce webhook
        $webhookContent = file_get_contents("php://input");
        $webhooks = json_decode($webhookContent, true);
        http_response_code(200);

        $order_id = $webhooks["data"]["id"];        
        $order = $api::getOrder($order_id);
        $bc_customer_id = $order->customer_id;
        $billing_email = $order->billing_address->email;

        // Make sure the order has been paid
        if ($order->status_id == 10 || $order->status_id == 11 && $bc_customer_id != 0) {

            $settings_query = "SELECT * FROM pts_settings WHERE customer_store_id = $customer_store_id";
            $settings = $this->db->queryFirst($settings_query);

            $customer_notes = $order->customer_message;

            $customer = $this->db->queryFirst("SELECT * FROM pts_customers WHERE bc_customer_id = $bc_customer_id AND customer_store_id = $customer_store_id");

            if ( $customer == null ) {
            // Let's create a profile for this customer and send out Welcome Email
                $this->sendWelcomeEmail($order);

                $pts_customer = array(
                  'customer_store_id' => $customer_store_id,
                  'bc_customer_id'    => $bc_customer_id,
                  'points_remaining'  => 0,
                  'first_name'        => $order->billing_address->first_name,
                  'last_name'         => $order->billing_address->last_name,
                  'email'             => $billing_email,
                  'referral_code'     => $this->generateRandomString(),
                  'create_time'       => time(),
                  'update_time'       => time()
                );

                $this->db->insert("pts_customers", $pts_customer);
             }

            // Check if the email has already placed an order and has already been got involved into a referral
           
            $referral_record = $this->db->queryFirst("SELECT * FROM pts_referrals WHERE referee_email = '$billing_email'");
            $order_record = $this->db->queryFirst("SELECT * FROM pts_orders WHERE email = '$billing_email'");

            if (strpos($customer_notes, 'referrer_code') && $order_record == null && $referral_record == null) {
            // this means the order is from a referral, the customer has not been registered in the store
            // first thing we need to do is to insert a new row into the referral table
                $referrer_code = substr($customer_notes, -6);
                
                $referrer = $this->db->queryFirst("SELECT * FROM pts_customers WHERE referral_code = '$referrer_code'");

                $bounty_reward = $settings['referrer_bounty'];
                $referral_ratio_reward = $order->subtotal_ex_tax * $settings['referrer_ratio'];
                $points_reward = $bounty_reward + $referral_ratio_reward;

                $referral_instance = array(
                    'customer_store_id'  => $customer_store_id,
                    'referrer_customer_id' => $referrer['bc_customer_id'],
                    'referrer_first_name' => $referrer['first_name'],
                    'referrer_last_name'  => $referrer['last_name'],
                    'referrer_email'      => $referrer['email'],
                    'referee_customer_id' => $bc_customer_id,
                    'referee_first_name' => $order->billing_address->first_name,
                    'referee_last_name'  => $order->billing_address->last_name,
                    'referee_email'      => $billing_email,
                    'bounty_reward'      => $bounty_reward,
                    'referral_ratio_reward'   => $referral_ratio_reward,
                    'points_reward'       => $points_reward,
                    'order_total'         => $order->subtotal_ex_tax, 
                    'order_id'            => $order->id,
                    'create_time'          => time(),
                    'update_time'          => time()  
                );

                $this->db->insert('pts_referrals', $referral_instance);

            // then we will need to add reward points to the referrer profile through pts_customers table using the referral_code
                
                $this->db->update("pts_customers", array('points_remaining' => $referrer['points_remaining'] + $points_reward), array("referral_code" => $referrer_code) );
            }

            $ratio = $settings["dollar_pts_ratio"];

            $cash_value = $order->subtotal_ex_tax - $order->store_credit_amount - $order->gift_certificate_amount - $order->discount_amount - $order->coupon_discount;

            $points_earned = $cash_value * $ratio;

            $points_used = ($order->store_credit_amount + $order->coupon_discount) * $ratio;

            $points_query = "SELECT * FROM pts_customers  WHERE customer_store_id = $customer_store_id AND bc_customer_id = $bc_customer_id";
            $points_remaining_array = $this->db->queryFirst($points_query);

            $points_remaining = $points_remaining_array["points_remaining"] + $points_earned - $points_used;

            // Update the customer table about the points_remaining
            $this->db->update("pts_customers", array('points_remaining' => $points_remaining), array('bc_customer_id' => $bc_customer_id, 'customer_store_id' => $customer_store_id));

            $pts_order = array(
                "customer_store_id"     => $customer_store_id,
                "bc_customer_id"        => $bc_customer_id,
                "total"                 => $order->subtotal_ex_tax,
                "cash_total"            => $cash_value,
                "order_id"              => $order->id,
                "first_name"            => $order->billing_address->first_name,
                "last_name"             => $order->billing_address->last_name,
                "email"                 => $order->billing_address->email,
                "points_received"       => $points_earned,
                "points_used"           => $points_used,
                "create_time"           => time(),
                "update_time"           => time(),
            );

             if(strpos($customer_notes, 'referrer_code')) {
                $pts_order["referrer_code"] = substr($customer_notes, -6);
              }

                $this->db->insert("pts_orders", $pts_order);

        } // End of the if statement of checking order status

    } // End of createPtsOrder function

/**
 * Send out welcome email if the order is the first order for a specifc customer
 *
 * @param $order - the order object from Big Commerce
 *
 * @return 
 *
 */

    public function sendWelcomeEmail($order)
    {   
        $bc_customer_id = $order->id;
        $order = $this->db->queryFirst("SELECT id FROM pts_order WHERE bc_customer_id = $bc_customer_id");

        if ($order != null) {
            $email_vars = array(
                'first_name'        => $order->billing_address->first_name,
                'last_name'         => $order->billing_address->last_name,
                'email'             => $order->billing_address->email,
            );
            $this->emailService->sendWelcomeEmail($email_vars);
        } else {
            return false;
        }
    }

/**
 * Generates a random string
 *
 * @param $length - the length of the desired string
 *
 * @return $randomString - the random string
 *
 */

    public function generateRandomString($length = 6)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
        
}







